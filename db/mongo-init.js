db = db.getSiblingDB('newtestdb')


db.createUser({
	user: 'user',
	pwd: 'pass123',
	roles: [
		{
			role: 'dbOwner',
			db: 'newtestdb',
		},
	],
});