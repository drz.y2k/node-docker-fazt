FROM node:18

# Create app directory
WORKDIR /usr/src/app

# copiamos nuestro package json a nuestro directorio de trabajo
COPY package*.json ./


# instalamos las dependencias
RUN npm install

# copiamos el resto de archivos a nuestro directorio de trabajo
COPY . .

# exponemos el puerto 3000
EXPOSE 3000

# iniciamos el servidor
CMD ["node", "server.js"]