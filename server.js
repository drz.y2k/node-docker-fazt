import express from 'express';
import mongoose from 'mongoose';
import { v4 } from 'uuid';

const app = express();

const db = await mongoose.connect("mongodb://user:pass123@mymongo/newtestdb")
console.log(db.connection.db.databaseName, '<- db name');

const ProductSchema = new mongoose.Schema({
	name: String
})

const ProductModel = mongoose.model('Product', ProductSchema);

app.get('/', async (req, res) => {
	const newProduct = await ProductModel.create({ name: 'desktop' });
	// res.send('Hello World');
	res.json({
		id: v4(),
		newProduct
	})
});

app.listen(3000);
console.log('Server on port', 3000);